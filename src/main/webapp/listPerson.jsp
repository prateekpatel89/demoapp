<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show All Students</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>Person ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th colspan="2">Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${persons}" var="person">
                <tr>
                    <td><c:out value="${person.id}" /></td>
                    <td><c:out value="${person.firstName}" /></td>
                    <td><c:out value="${person.lastName}" /></td>
                    <td><c:out value="${person.email}" /></td>
                    <td><a
                        href="PersonController.do?action=edit&id=<c:out value="${person.id }"/>">Update</a></td>
                    <td><a
                        href="PersonController.do?action=delete&id=<c:out value="${person.id }"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p>
        <a href="PersonController.do?action=insert">Add Person</a>
    </p>
</body>
</html>