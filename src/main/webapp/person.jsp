<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 
<title>Add New Person</title>
</head>
<body>
    <form action="PersonController.do" method="post">
        <fieldset>
            <div>
                <label for="personId">Person ID</label> <input type="text"
                    name="id" value="<c:out value="${person.id}" />"
                    readonly="readonly" placeholder="Person ID" />
            </div>
            <div>
                <label for="firstName">First Name</label> <input type="text"
                    name="firstName" value="<c:out value="${person.firstName}" />"
                    placeholder="First Name" />
            </div>
            <div>
                <label for="lastName">Last Name</label> <input type="text"
                    name="lastName" value="<c:out value="${person.lastName}" />"
                    placeholder="Last Name" />
            </div>
            <div>
                <label for="email">Email</label> <input type="text" name="email"
                    value="<c:out value="${person.email}" />" placeholder="email" />
            </div>
            <div>
                <input type="submit" value="Submit" />
            </div>
        </fieldset>
    </form>
</body>
</html>