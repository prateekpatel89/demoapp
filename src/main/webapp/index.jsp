<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome</title>
</head>
<body>
<form action="PersonController.do" method="get">
	 <div>
		  <input type="hidden" name="action" value="search"/>
		  <input type="text" name="criteriaInput"  placeholder="Search People" /> 
		 <select name="criteria">
			  <option value="firstName">Name</option>
			  <option value="email">Email</option>
		</select> 
		<input type="submit" value="Search" />
	 </div>
       
</form>
	 <p>
        <a href="PersonController.do?action=insert">Add Person</a>
     </p>
     <p>
        <a href="PersonController.do?action=listPerson">View All Records</a>
     </p>
</body>
</html>