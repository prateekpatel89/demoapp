package com.demo.dao;

import java.util.ArrayList;
import java.util.List;

import com.demo.model.Person;
import com.demo.util.DBUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PersonDaoImpl implements PersonDao{
	
	private Connection conn;
	
	public PersonDaoImpl() {
        conn = DBUtil.getConnection();
    }

	public void addPerson(Person person) {
		try {
            String query = "insert into person (firstName, lastName, email) values (?,?,?)";
            PreparedStatement preparedStatement = conn.prepareStatement( query );
            preparedStatement.setString( 1, person.getFirstName() );
            preparedStatement.setString( 2, person.getLastName() );
            preparedStatement.setString( 3, person.getEmail() );
            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
	}

	public void deletePerson(int id) {
		try {
            String query = "delete from person where id=?";
            PreparedStatement preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
	}

	public void updatePerson(Person person) {
		 try {
	            String query = "update person set firstName=?, lastName=?, email=? where id=?";
	            PreparedStatement preparedStatement = conn.prepareStatement( query );
	            preparedStatement.setString( 1, person.getFirstName() );
	            preparedStatement.setString( 2, person.getLastName() );
	            preparedStatement.setString( 3, person.getEmail() );
	            preparedStatement.setInt(4, person.getId());
	            preparedStatement.executeUpdate();
	            preparedStatement.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	}

	public Person getPersonById(int id) {
		 Person person = new Person();
	        try {
	            String query = "select * from person where id=?";
	            PreparedStatement preparedStatement = conn.prepareStatement( query );
	            preparedStatement.setInt(1, id);
	            ResultSet resultSet = preparedStatement.executeQuery();
	            while( resultSet.next() ) {
	            	person.setId( resultSet.getInt( "id" ) );
	            	person.setFirstName( resultSet.getString( "firstName" ) );
	            	person.setLastName( resultSet.getString( "lastName" ) );
	            	person.setEmail(resultSet.getString( "email" ));
	            }
	            resultSet.close();
	            preparedStatement.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return person;
	    };
	

	public List<Person> getAllPersons() {
		List<Person> persons = new ArrayList<Person>();
        try {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery( "select * from person" );
            while( resultSet.next() ) {
            	Person person = new Person();
            	person.setId( resultSet.getInt( "id" ) );
            	person.setFirstName( resultSet.getString( "firstName" ) );
            	person.setLastName( resultSet.getString( "LastName" ) );
            	person.setEmail(resultSet.getString( "email" ));
            	persons.add(person);
            }
            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
	}
	
	public List<Person> searchPerson(String criteria, String input) {
		List<Person> persons = new ArrayList<Person>();
		String query="";
        try {
        	if( criteria.equalsIgnoreCase( "firstName" ) ){
        		 query = "select * from person where firstName like ?";
        	}
        	else{
        		 query = "select * from person where email like ?";
        	}
        	
            PreparedStatement preparedStatement = conn.prepareStatement( query );
            preparedStatement.setString( 1, input+"%");
            System.out.println(preparedStatement);
          
            ResultSet resultSet = preparedStatement.executeQuery();
            
            while( resultSet.next() ) {
            	Person person = new Person();
            	person.setId( resultSet.getInt( "id" ) );
            	person.setFirstName( resultSet.getString( "firstName" ) );
            	person.setLastName( resultSet.getString( "LastName" ) );
            	person.setEmail(resultSet.getString( "email" ));
            	persons.add(person);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return persons;
	}
	

}
