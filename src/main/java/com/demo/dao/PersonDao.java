package com.demo.dao;

import java.util.List;

import com.demo.model.Person;

public interface PersonDao {
	
	public void addPerson(Person person);
	
	public void deletePerson(int id);
	
	public void updatePerson( Person person);
	
	public Person getPersonById(int id);
	
	public List<Person> getAllPersons();
	
	public List<Person> searchPerson(String criteria, String input);

}
