package com.demo.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.dao.PersonDao;
import com.demo.dao.PersonDaoImpl;
import com.demo.model.Person;

@WebServlet("/PersonController")
public class PersonController extends HttpServlet{

	
	private static final long serialVersionUID = 1L;
	
	private PersonDao dao;
	
	public static final String LIST_PERSON = "/listPerson.jsp";
	
	public static final String INSERT_OR_EDIT = "/person.jsp";
	
	public static final String SEARCH_PERSON = "/searchPerson.jsp";
	
	public  PersonController(){
		dao = new PersonDaoImpl();
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter( "action" );
 
        if( action.equalsIgnoreCase( "delete" ) ) {
            forward = LIST_PERSON;
            int id = Integer.parseInt( request.getParameter("id") );
            dao.deletePerson(id);
            request.setAttribute("persons", dao.getAllPersons() );
        }
        else if( action.equalsIgnoreCase( "edit" ) ) {
            forward = INSERT_OR_EDIT;
            int id = Integer.parseInt( request.getParameter("id") );
            Person person = dao.getPersonById(id);
            request.setAttribute("person", person);
        }
        else if( action.equalsIgnoreCase( "insert" ) ) {
            forward = INSERT_OR_EDIT;
        }
        else if( action.equalsIgnoreCase( "search" ) ) {
            forward = LIST_PERSON;
            String criteria = request.getParameter("criteria") ;
            String input= request.getParameter("criteriaInput") ;
            request.setAttribute("persons", dao.searchPerson(criteria,input) );
        }
        else {
            forward = LIST_PERSON;
            request.setAttribute("persons", dao.getAllPersons() );
        }
        RequestDispatcher view = request.getRequestDispatcher( forward );
        view.forward(request, response);
    }
 
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Person person = new Person();
        person.setFirstName( request.getParameter( "firstName" ) );
        person.setLastName( request.getParameter( "lastName" ) );
        person.setEmail(request.getParameter( "email" ));
       
        String id = request.getParameter("id") ;
        System.out.println(request);
 System.out.println("1");
 System.out.println(id);
        if( id == null || id.isEmpty() ){
        	 System.out.println("2");
            dao.addPerson(person);
        }
        else {
        	 System.out.println("3");
        	person.setId( Integer.parseInt(id) );
            dao.updatePerson(person);
        }
        RequestDispatcher view = request.getRequestDispatcher( LIST_PERSON );
        request.setAttribute("persons", dao.getAllPersons());
        view.forward(request, response);
    }

}
